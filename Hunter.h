#pragma once
#include "Entity.h"
#include "vector"
#include <thread>         
#include <chrono> 

class GameField;

class Hunter :public Entity
{


public:
	Hunter(std::string name, GameField &gameField, size_t x, size_t y);

	bool isDangerous() const;

	void move(GameField &gameField, size_t x, size_t y);

	bool step(GameField &gameField, int randDirection);


};
