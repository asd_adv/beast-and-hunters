#include "GameField.h"
#include "FieldObject.h"
#include "Entity.h"



GameField::GameField(size_t size, int spriteSize) :size_(size), spriteSize_(spriteSize)
{
	playingField_.resize(size);
	for (size_t i = 0; i < size_; i++)
	{
		playingField_[i].resize(size);
	}
}

size_t GameField::size()
{
	return size_;
}

size_t GameField::spriteSize()
{
	return spriteSize_;
}

void GameField::draw(sf::RenderWindow &window)
{



	for (size_t i = 0; i < size_; i++)
	{
		for (size_t j = 0; j < size_; j++)
		{
			if ((playingField_[i][j].field != nullptr))
			{	
				playingField_[i][j].field->setPositionFirst(i*spriteSize_, j*spriteSize_);
				window.draw(playingField_[i][j].field->getFirstSprite());
			}
			
			if ((playingField_[i][j].entity != nullptr))
			{
				if (playingField_[i][j].entity->isSelected())
				{
					playingField_[i][j].entity->setPositionSecond(i*spriteSize_, j*spriteSize_);
					window.draw(playingField_[i][j].entity->getSecondSprite());					
				}
				else
				{
					playingField_[i][j].entity->setPositionFirst(i*spriteSize_, j*spriteSize_);
					window.draw(playingField_[i][j].entity->getFirstSprite());
				}
			
			}
		}		
	}
}