#pragma once
#include "FieldObject.h"
#include "vector"

class GameField;

class Trap:public FieldObject
{

public:
	Trap(std::string name, GameField &gameField, size_t x, size_t y);

	bool isDangerous() const;


	void move(GameField &gameField, size_t x, size_t y);


};