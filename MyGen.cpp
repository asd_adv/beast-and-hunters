#include "MyGen.h"

MyGen::MyGen(int a, int b) : gen_(time(0)), rand_(a, b) {};

int MyGen::next()
{
	return rand_(gen_);
};