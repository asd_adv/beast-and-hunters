#pragma once
#include "Cell.h"
#include "GameObject.h"
#include <string>
#include <SFML/Graphics.hpp>
#include <vector>

class Entity;
class FieldObject;

struct GameField
{
	std::vector<std::vector<Cell>> playingField_;
	size_t size_;
	int spriteSize_;

	GameField(size_t size,int spriteSize);

	size_t spriteSize();

	size_t size();

	void draw(sf::RenderWindow &window);

};
