#pragma once
#include "FieldObject.h"
#include "vector"

class GameField;

class Grass :public FieldObject
{

public:
	Grass(std::string name, GameField &gameField, size_t x, size_t y);

	bool isDangerous() const;

	void move(GameField &gameField, size_t x, size_t y);


};